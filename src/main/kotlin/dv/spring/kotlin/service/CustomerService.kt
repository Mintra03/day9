package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvince(province: String): List<Customer>
    fun getCustomerByUserStatus(userStatus: UserStatus): List<Customer>
    fun save(customer: Customer): Customer
    fun save(address: Long, mapCustomerDto: Customer): Customer
    fun remove(id: Long): Customer?
//    fun findByBoughtProduct(name: String): List<Customer>
}