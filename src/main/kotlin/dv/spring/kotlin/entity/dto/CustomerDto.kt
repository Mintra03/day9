package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.User
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.security.entity.Authority

data class CustomerDto(
        var name: String? = null,
        var email: String? = null,
//        var username: String? = null,
//        var authorities: List<AuthorityDto> = mutableListOf(),
        var defaultAddress: Address? = null,
        var userStatus: UserStatus? = UserStatus.PENDING

)