package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.WAIT,
        var selectedProducts : List<SelectedProductDto>? = null,
        var customer: CustomerDto? = null
)