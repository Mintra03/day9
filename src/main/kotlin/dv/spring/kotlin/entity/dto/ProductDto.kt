package dv.spring.kotlin.entity.dto

import sun.security.x509.AccessDescription
import java.time.temporal.TemporalAmount

data class ProductDto
(
        var name: String? = null,
        var description: String? = null,
        var price: Double? = null,
        var amountInStock:Int? = null,
        var imageUrl: String?=null,
        var manu: ManufacturerDto? = null,
        var id: Long ? = null
)