package dv.spring.kotlin.entity.dto

data class ShoppingCartCustomerProductDto(var customerName: DisplayCustomer? = null,
                                          var selectedProduct: List<DisplaySelectedProduct>? = null)

data class DisplayCustomer(var customerName:String? = null)

data class DisplaySelectedProduct (var productname: List<DisplayProduct>? = null,
                                   var quantity: Int? = null)

data class DisplayProduct(var productName: String? = null)




//package dv.spring.kotlin.entity.dto
//
//import dv.spring.kotlin.entity.Address
//
//data class ShoppingCustomerDto(
//        var customer: String? = null,
//        var address: Address? = null,
//        var products: List<DisplayProduct>? = null
//)
//
////data class DisplayProduct (
////        var name: String? = null,
////        var description: String? = null,
////        var quantity: Int? = null
////)
////
////data class PageShoppingCustomerDto (
////        var totalPage: Int,
////        var totalElement: Long,
////        var shoppingList: List<ShoppingCustomerDto>
////)