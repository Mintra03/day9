package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne

@Entity
data class Customer2 (
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus? = UserStatus.PENDING,
        var isDeleted: Boolean = false
) : User(name, email, userStatus) {

    @ManyToMany
    var shippingAddress = mutableListOf<Address>()
    @ManyToOne
    var billingAddress: Address? = null
    @ManyToOne
    var defaultAddress: Address? = null
//    @OneToMany
//    var shoppingCart : ShoppingCart? = null
}