package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoOBImpl : AddressDao {
    override fun findById(addressId: Long): Address? {
        return addressDaoRepository.findById(addressId).orElse(null)
    }

    override fun save(address: Address): Address {
        return addressDaoRepository.save(address)
    }

    override fun getAddresses(): List<Address> {
        return addressDaoRepository.findAll().filterIsInstance(Address::class.java)
    }


    @Autowired
    lateinit var addressDaoRepository: AddressRepository
}
