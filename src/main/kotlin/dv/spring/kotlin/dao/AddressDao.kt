package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address

interface AddressDao {
    fun getAddresses(): List<Address>
    fun save(address: Address): Address
    fun findById(addressId: Long): Address?
}