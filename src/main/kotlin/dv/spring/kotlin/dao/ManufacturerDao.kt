package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired

interface ManufacturerDao {
    fun getManufacturers():List<Manufacturer>
    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(id: Long): Manufacturer?

}