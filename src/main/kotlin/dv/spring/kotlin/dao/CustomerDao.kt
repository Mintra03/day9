package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus

interface CustomerDao {
    fun getCustomer(): List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvinceName(province: String): List<Customer>
    fun getCustomerByUserStatus(userStatus: UserStatus): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?
}