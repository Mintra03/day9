package dv.spring.kotlin.Controller

import dv.spring.kotlin.entity.dto.ManufacturerDto
import dv.spring.kotlin.service.ManufacturerService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ManufacturerController{
    @Autowired
    lateinit var manufacturerService: ManufacturerService
//    @GetMapping("/manufacturer")
//    fun getAllManufacturer():ResponseEntity<Any>{
//        return ResponseEntity.ok(manufacturerService.getManufacturer());
//    }

    @PostMapping("/manufacturer")
    fun addManufacturer(@RequestBody manu: ManufacturerDto)
        : ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }

    @PostMapping("/manufacturer01")
    fun updateManufacturer(@RequestBody manu: ManufacturerDto)
        : ResponseEntity<Any> {
        if (manu.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("id mustnot be null")
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer(
                        manufacturerService.save(manu)
                )
        )
    }

    @PutMapping("/manufacturer/{manuId}")
    fun updateManufacturer(@PathVariable("manuId") id:Long?,
                           @RequestBody manu: ManufacturerDto)
        : ResponseEntity<Any> {
        manu.id = id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer(
                        manufacturerService.save(manu)
                )
        )
    }


}