package dv.spring.kotlin.Controller

import dv.spring.kotlin.entity.dto.PageSelectedProductDto
import dv.spring.kotlin.service.SelectedProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SelectedProductController {
    @Autowired
    lateinit var selectedProductService: SelectedProductService

    @GetMapping("/selectedProduct/name")
    fun getSelectedProduct() : ResponseEntity<Any> {
        val selectedProduct = selectedProductService.getSelectedProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSelectedProductDto(selectedProduct))
    }

    @GetMapping("/selectedProduct/query")
    fun getSelectedProducts(@RequestParam("product") product: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapSelectedProductDto(selectedProductService.getSelectedProductByProduct_Name(product))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }

    @GetMapping("/selectedProducts/partialQuery")
    fun getSelectedProductsPartial(@RequestParam("product") product: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapSelectedProductDto(selectedProductService.getSelectedProductByPartialName(product))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/selectedProduct/partialQuery/and")
    fun getSelectedProductsPartial(@RequestParam("product") product: String,
                                   @RequestParam("quantity", required = false) quantity: Int?): ResponseEntity<Any> {
        val output: List<Any>
        if (quantity == null) {
            output = MapperUtil.INSTANCE.mapSelectedProductDto(selectedProductService.getSelectedProductByPartialName(product))
        } else {
            output = MapperUtil.INSTANCE.mapSelectedProductDto(selectedProductService.getSelectedProductByPartialNameAndQuantity(product, quantity))
        }
        return ResponseEntity.ok(output)
    }

    @GetMapping("/selectedProduct/product")
    fun getSelectedProductWitchPage(@RequestParam("product") product: String,
                                    @RequestParam("page") page:Int,
                                    @RequestParam("pageSize") pageSize:Int):ResponseEntity<Any>{
        val output = selectedProductService.getSelectedProductWithPage(product, page, pageSize)
        return ResponseEntity.ok(PageSelectedProductDto(
                totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapSelectedProductDto(output.content)
        ))

    }


}

