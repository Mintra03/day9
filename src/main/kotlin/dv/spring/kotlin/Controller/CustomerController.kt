package dv.spring.kotlin.Controller

import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.persistence.Id
import kotlin.coroutines.experimental.buildIterator

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService : CustomerService
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping ("/customer")
    fun getCustomer(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name") name:String) :ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/partialQuery")
    fun getCustomerPartial(@RequestParam("name") name:String,
                           @RequestParam(value = "email",required = false) email: String?): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name, name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customerAddress")
    fun getCustomerByProvince(@RequestParam("province") province: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByProvince(province))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/userStatus")
    fun getCustomerByUserStatus(@RequestParam("userStatus") userStatus: UserStatus): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByUserStatus(userStatus))
        return ResponseEntity.ok(output)
    }


    @GetMapping("/customer/{productName}")
    fun getCustomerByProductName(@RequestParam("name") name: String): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomerProductDto(
                shoppingCartService.getCustomerByProductName(name))
        return ResponseEntity.ok(output)
    }

    @PostMapping("/customer/newCustomer")
    fun addAddressCustomer(@RequestBody customerDto: CustomerDto)
        : ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/newCustomer/{addressId}")
    fun addAddress(@RequestBody customerDto: CustomerDto,
                   @PathVariable addressId: Long)
            : ResponseEntity<Any> {
        val output = customerService.save(addressId,
                MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deletedCustomer(@PathVariable("id") id:Long): ResponseEntity<Any> {
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }

}