package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository: CrudRepository<SelectedProduct,Long>
{
    fun findByProduct_NameContainingIgnoreCase(name: String, pageable: Pageable): Page<SelectedProduct>
    fun findByProduct_NameContainingIgnoreCaseOrQuantityContaining(product: String, quantity: Int): List<SelectedProduct>
    fun findByProduct_Name(product: String): SelectedProduct
    fun findByProduct_NameContainingIgnoreCase(product: String): List<SelectedProduct>

}