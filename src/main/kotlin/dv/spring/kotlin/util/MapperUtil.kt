package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.security.entity.Authority
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer",target= "manu")
    )

    fun mapProductDto(product: Product?):ProductDto?
    fun mapProductDto(products: List<Product>): List<ProductDto>
    fun mapManufacturer(manu: Manufacturer) : ManufacturerDto

//    @Mappings(
//            Mapping(source = "customer.jwtUser.username", target = "username"),
//            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
//    )
    fun mapCustomerDto (customer: Customer?) : CustomerDto?
    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>

    fun mapShoppingCartDto(shoppingCart: ShoppingCart) : ShoppingCartDto
    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>) : List<ShoppingCartDto>

    fun mapSelectedProductDto(selectedProduct: SelectedProduct?) : SelectedProductDto?
    fun mapSelectedProductDto(selectedProducts: List<SelectedProduct>) : List<SelectedProductDto>



//    @Mappings(
//            Mapping(source = "product.name", target = "name"),
//            Mapping(source = "product.description", target= "description"),
//            Mapping(source = "quantity",target= "quantity")
//    )
//    fun mapProductDisPlay(selectedProduct: SelectedProduct): DisplayProduct
//    fun mapProductDisplay(selectedProducts: List<SelectedProduct>): List<DisplayProduct>
//
//    @Mappings(
//            Mapping(source = "customer.name",target= "customer"),
//            Mapping(source = "customer.defaultAddress", target= "address"),
//            Mapping(source = "selectedProducts",target= "products")
//    )
//    fun mapShoppingCartCustomerDao(shoppingCart: ShoppingCart): ShoppingCustomerDto
//    fun mapShoppingCartCustomerDao(shoppingCarts: List<ShoppingCart>): List<ShoppingCustomerDto>

    fun mapAddress(address: Address) : AddressDto

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto) : Manufacturer

    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto): Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )
    fun mapCustomer2Dto(customer: Customer): Customer2Dto

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

    fun mapCustomerProductDto(shoppingCart: ShoppingCart): CustomerProductDto
    fun mapCustomerProductDto(shoppingCarts: List<ShoppingCart>): List<CustomerProductDto>

    @InheritInverseConfiguration
    fun mapShoppingCart(shoppingCart: ShoppingCartDto): ShoppingCart

    fun mapMyShoppingCart(shoppingCart: ShoppingCart): MyShoppingCartDto

    fun mapMyShoppingCart(shoppingCart: List<ShoppingCart>): List<MyShoppingCartDto>

    @InheritInverseConfiguration
    fun mapMyShoppingCart(shoppingCart: MyShoppingCartDto): ShoppingCart

}